# README

## 要件

- python 3.10以上
- GPU推奨（CPUでも動作しますが、処理に時間がかかります）

## インストールと起動

###  Windows

1. `setup.bat`を実行
2. `torch, cuda導入・初期設定 以下のコマンドを実行`
```bash
cd detection-ai-mozaic-interface
venv\Scripts\Activate
# pytorchのインストールは使用するPCのGPUに合わせてください -> [要件はこちら](https://pytorch.org/get-started/locally/)
pip install torch==2.0.1+cu118 torchvision==0.15.2+cu118 torchaudio==2.0.2 --index-url https://download.pytorch.org/whl/cu118
```
3. run.batを実行して起動

### Mac

```bash
# インストール
cd detection-ai-mozaic-interface
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

```bash
# 起動
cd detection-ai-mozaic-interface
source venv/bin/activate
python main.py
```