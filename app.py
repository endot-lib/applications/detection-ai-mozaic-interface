import gradio as gr
import torch
import os
import subprocess
from termcolor import colored
from common import labels as l
from common.process_route import process_route
from common.util import load_models
from common.params import ProcessingParams
from ultralytics import YOLO

def wrapper_fn(
        selected_model,
        segment_model,
        img,
        auto_mosaic,
        mosaic_size,
        mosaic_threshold,
        expand_mosaic_range,
        bulk_mode,
        input_dir,
        output_dir,
    ):

    # モデルの読み込み
    model = YOLO(model_options[selected_model]).to(device)
    segment_model = YOLO(segment_model_options[segment_model]).to(device)

    model.model_name = model_options[selected_model]  # モデルオブジェクトに名前を設定
    params = ProcessingParams(
        model=model,
        segment_model=segment_model,
        auto_mosaic=auto_mosaic,
        mosaic_size=mosaic_size,
        mosaic_threshold=mosaic_threshold,
        expand_mosaic_range=expand_mosaic_range,
        device=device,
        bulk_mode=bulk_mode,
        input_dir=input_dir,
        output_dir=output_dir,
    )
    return process_route(params, img)

def create_interface():
    with gr.Blocks(
            title="Detection AI moZaic Interface",
            css="./custom.css"
        ) as blocks:
        gr.Markdown("### Detection AI moZaic Interface")  # タイトルとしてMarkdownを使用
        with gr.Row():
            with gr.Column():
                selected_model = gr.Dropdown(choices=list(model_options.keys()), label=l.SELECT_MODEL_LABEL, value=default_model_name)
                segment_model = gr.Dropdown(choices=list(segment_model_options.keys()), label=l.SEGMENT_MODEL_LABEL, value=default_segment_model_name)
                img = gr.Image(type="numpy", label=l.INPUT_IMAGE_LABEL)
                auto_mosaic = gr.Checkbox(label=l.AUTO_MOSAIC_LABEL, info=l.AUTO_MOSAIC_PLACEHOLDER)
                mosaic_size = gr.Slider(minimum=2, maximum=30, value=5, step=1, label=l.MOSAIC_SIZE_LABEL)
                mosaic_threshold = gr.Slider(minimum=0, maximum=1, value=0.5, step=0.05, label=l.MOSAIC_THRESHOLD_LABEL, info=l.MOSAIC_THRESHOLD_TEXT)
                expand_mosaic_range = gr.Slider(minimum=0, maximum=100, value=0, step=1, label=l.EXPAND_MOSAIC_RANGE_LABEL, info=l.EXPAND_MOSAIC_RANGE_PLACEHOLDER)
                with gr.Group():
                    bulk_mode = gr.Checkbox(label=l.BULK_MODE_LABEL, info=l.BULK_MODE_PLACEHOLDER)
                    input_dir = gr.Textbox(label=l.INPUT_DIR_LABEL, placeholder=l.INPUT_DIR_PLACEHOLDER)
                    output_dir = gr.Textbox(label=l.OUTPUT_DIR_LABEL, placeholder=l.OPUTPUT_DIR_PLACEHOLDER)
                submit_button = gr.Button(value="Submit", variant="primary")
            with gr.Column():
                output_img = gr.Image(type='numpy', label=l.MOSAIC_IMAGE_LABEL)
                result_message = gr.HTML(label=l.RESULT_MESSAGE_LABEL)
                processing_progress = gr.Textbox(label=l.PROCESSING_PROGRESS_LABEL)

        submit_button.click(
            wrapper_fn,
            inputs=[
                selected_model,
                segment_model,
                img,
                auto_mosaic,
                mosaic_size,
                mosaic_threshold,
                expand_mosaic_range,
                bulk_mode,
                input_dir,
                output_dir,
            ],
            outputs=[output_img, result_message, processing_progress]
        )

    return blocks

if __name__ == "__main__":
    # 現在のコミットハッシュを表示
    commit_hash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip().decode('utf-8')
    print(colored(f"Current commit hash: {commit_hash}", 'cyan'))

    # 初期設定 app.py のあるディレクトリのパスを取得
    current_dir = os.path.dirname(os.path.abspath(__file__))

    # detectモデルの読み込み
    models_dir = os.path.join(current_dir, 'models/detect')
    default_model_name, default_model_path, model_options = load_models(models_dir)

    # segmentモデルの読み込み
    segment_models_dir = os.path.join(current_dir, 'models/segment')
    default_segment_model_name, default_segment_model_path, segment_model_options = load_models(segment_models_dir)

    gpu_status = "available" if torch.cuda.is_available() else "not available"
    # Set the device for PyTorch
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(colored('=== Device Information ===', 'green'))
    print(colored(f"GPU is {gpu_status}", 'green'))
    print(colored(f"using device is {device}", 'green'))

    interface = create_interface()
    interface.launch(server_port=4701)