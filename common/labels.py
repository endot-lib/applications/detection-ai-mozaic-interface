# 定数やテキスト、ラベル等

# 定数
DEFAULT_AUTO_MOZAIC_SIZE = 4
MOSAIC_TARGETS = {
    0: 'manko',
    1: 'chinko',
    2: 'tama',
    # 3: 'sounyu',
    }  # モザイク対象の物体名
DETECT_MOSAIC_TARGETS = {
    0: 'manko',
}  # モザイク対象の物体名
SEGMENT_MOSAIC_TARGETS = {
    1: 'chinko',
    2: 'tama',
}  # モザイク対象の物体名
SEGMENT_MODEL_LABELS = {
    0: 'chinko',
} # セグメントモデルのラベル名

# ラベル系
SELECT_MODEL_LABEL = "Select Model"
SEGMENT_MODEL_LABEL = "Segment Model"
INPUT_IMAGE_LABEL = "Input Image"
AUTO_MOSAIC_LABEL = "Auto Mosaic Size"
MOSAIC_SIZE_LABEL = "Mosaic Size"
MOSAIC_THRESHOLD_LABEL = "Mosaic Threshold"
EXPAND_MOSAIC_RANGE_LABEL = "Expand Mosaic Range"
BULK_MODE_LABEL = "Bulk Mode"
INPUT_DIR_LABEL = "Input Directory"
OUTPUT_DIR_LABEL = "Output Directory"
MOSAIC_IMAGE_LABEL = "Result"
RESULT_MESSAGE_LABEL = "Result Message"
PROCESSING_PROGRESS_LABEL = "Processing Progress"

# テキスト系
AUTO_MOSAIC_PLACEHOLDER = "最小4ピクセル平方モザイクかつ画像全体の長辺が400ピクセル以上の場合、必要部位に「画像全体長辺*1/100」程度を算出 by DMM"
MOSAIC_THRESHOLD_TEXT = "物体を検出する閾値を設定します（値が低いほどゆるく検出、高いほど厳しく検出します）"
EXPAND_MOSAIC_RANGE_PLACEHOLDER = "モザイクの範囲を指定した値px分拡張します。（※Auto mosaic sizeにチェックがある場合、指定された値%拡張されます）"
INPUT_DIR_PLACEHOLDER = "example C:\\directory\\to\\path\\from"
OPUTPUT_DIR_PLACEHOLDER = "example C:\\directory\\to\\path\\to"
BULK_MODE_PLACEHOLDER = "ディレクトリを指定して一括処理を行う場合はチェックしてください"