import glob
import os
class ProcessingParams:
    def __init__(self, model, segment_model, auto_mosaic, mosaic_size, mosaic_threshold, expand_mosaic_range, device, bulk_mode, input_dir, output_dir):
        self.model = model
        self.segment_model = segment_model
        self.auto_mosaic = auto_mosaic
        self.mosaic_size = mosaic_size
        self.mosaic_threshold = mosaic_threshold
        self.expand_mosaic_range = expand_mosaic_range
        self.device = device
        self.bulk_mode = bulk_mode
        self.input_dir = input_dir
        self.output_dir = output_dir

    def get_image_files(self, extensions=("jpg", "png", "gif", "mp4")):
        image_files = []
        for ext in extensions:
            image_files.extend(glob.glob(os.path.join(self.input_dir, f"*.{ext}")))
        return image_files