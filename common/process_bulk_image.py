"""
バルクモードが選択された場合に実行される処理
jpg, png, gif, mp4等ファイルタイプによって処理方法が違うので
それぞれのファイルタイプに対応する処理関数へ投げる役割
共通する処理はここで実行
"""
import os
from termcolor import colored
from collections import defaultdict
from common.process_gifs import process_gifs
from common.process_mp4s import process_mp4s
from common.process_images import process_images


def _classify_files(image_files):
    """
    ファイルを拡張子で分類
    """
    file_groups = defaultdict(list)
    ext_to_group = {
        '.jpg': 'jpg_png',
        '.png': 'jpg_png',
        '.gif': 'gif',
        '.mp4': 'mp4',
    }
    for image_file in image_files:
        _, ext = os.path.splitext(image_file)
        group = ext_to_group.get(ext.lower(), 'unknown')
        if group != 'unknown':
            file_groups[group].append(image_file)
    return file_groups

def process_group(params, file_group, ext_type):
    """
    ファイルグループを処理（拡張子ごとにまとめられたファイルを処理関数へ投げる）
    """
    mosaic_img = None
    if ext_type == 'jpg_png':
        mosaic_img = process_images(params, file_group)
    # if ext_type == 'gif':
    #     return process_gifs(params, file_group)
    # if ext_type == 'mp4':
    #     return process_mp4s(params, file_group)
    return mosaic_img


def process_bulk_images(params):
    """
    一括処理メイン関数
    """
    os.makedirs(params.output_dir, exist_ok=True)  # 出力ディレクトリ作成
    image_files = params.get_image_files()  # 画像ファイル一覧取得
    total_images = len(image_files)
    print(colored(f"Total Files: {total_images}", "green"))

    # 拡張子ごとに処理
    for ext_type, files in _classify_files(image_files).items():
        mosaic_img = process_group(params, files, ext_type)

    # UI上に出力するメッセージ
    message = f"Completed Process {total_images} images."
    print(message)
    return mosaic_img, message, f"{total_images}/{total_images}"