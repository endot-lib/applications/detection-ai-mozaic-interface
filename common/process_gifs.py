import os
import imageio
import numpy as np
from tqdm import tqdm
import cv2
from typing import List, Tuple, Optional
from common.util import apply_circular_mosaic
from common.params import ProcessingParams

def interpolate_detection_flags(detection_flags: List[int], window_size: int = 3) -> List[int]:
    """物体検出が漏れたフレームを見つけ、それらにモザイク処理を行うためのフラグを立てる関数"""
    interpolated_flags: List[int] = detection_flags.copy()
    half_window: int = window_size // 2

    for i in range(len(detection_flags)):
        if detection_flags[i] == 0:
            window_range: List[int] = detection_flags[max(0, i - half_window):min(len(detection_flags), i + half_window + 1)]
            if sum(window_range) > 0:
                interpolated_flags[i] = 1

    return interpolated_flags

def interpolate_boxes(
    frame_results: List[Tuple[np.ndarray, List[List[float]]]],
    detection_flags: List[int],
    interpolated_flags: List[int]
) -> List[Tuple[np.ndarray, List[List[float]]]]:
    """フラグが補完されたフレームにはバウンディングボックス情報がないので追加する関数"""
    interpolated_boxes: List[Tuple[np.ndarray, List[List[float]]]] = []

    def find_nearest_boxes(
        frame_results: List[Tuple[np.ndarray, List[List[float]]]],
        detection_flags: List[int],
        start_idx: int
    ) -> Tuple[List[List[float]], Optional[str]]:
        """最も近いフレームのバウンディングボックスを探す関数"""
        distance: int = 1
        while distance < len(frame_results):
            prev_idx: int = max(start_idx - distance, 0)
            next_idx: int = min(start_idx + distance, len(frame_results) - 1)

            if detection_flags[prev_idx] == 1:
                return frame_results[prev_idx][1], 'prev'

            if detection_flags[next_idx] == 1:
                return frame_results[next_idx][1], 'next'

            distance += 1

        return [], None

    def average_boxes(boxes1: List[List[float]], boxes2: List[List[float]]) -> List[List[float]]:
        """バウンディングボックスの平均を計算する関数"""
        return [((np.array(b1) + np.array(b2)) / 2.0).tolist() for b1, b2 in zip(boxes1, boxes2)]

    # フラグが補完されたフレームにはバウンディングボックス情報がないので追加する
    for i in range(len(frame_results)):
        frame_rgb: np.ndarray
        boxes: List[List[float]]
        frame_rgb, boxes = frame_results[i]

        if interpolated_flags[i] == 1:
            if detection_flags[i] == 1:
                interpolated_boxes.append((frame_rgb, boxes))
            else:
                nearest_boxes: List[List[float]]
                position: Optional[str]
                # フラグが立っていないフレームには最も近いフレームのバウンディングボックスを探して追加する
                nearest_boxes, position = find_nearest_boxes(frame_results, detection_flags, i)
                if position == 'prev':
                    interpolated_boxes.append((frame_rgb, nearest_boxes))
                elif position == 'next':
                    interpolated_boxes.append((frame_rgb, nearest_boxes))
                elif position == 'both':
                    box_set: List[List[float]] = average_boxes(nearest_boxes, frame_results[i + 1][1])
                    interpolated_boxes.append((frame_rgb, box_set))
                else:
                    # 両方のフレームにバウンディングボックスがない場合は何もしない
                    interpolated_boxes.append((frame_rgb, []))
        else:
            # フラグが立っていないフレームは何もしない
            interpolated_boxes.append((frame_rgb, boxes))

    return interpolated_boxes

def process_gif_frame(params: ProcessingParams, frame: np.ndarray) -> Tuple[List[List[float]], int]:
    """GIFの各フレームを処理する関数"""
    frame_rgb: np.ndarray = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    result = params.model(frame_rgb)

    boxes_and_class_id: List[List[float]] = [
        bbox.tolist() + [int(label.item())] for label, bbox in zip(result.pred[0][:, -1], result.pred[0][:, :-2])
    ]
    flag: int = 1 if len(result.pred[0]) > 0 else 0

    return boxes_and_class_id, flag

def process_gifs(params: ProcessingParams, file_group: List[str]) -> np.ndarray:
    """GIFファイルの処理を行う関数"""
    first_processed_frame: Optional[np.ndarray] = None

    for gif_path in tqdm(file_group, desc="Processing GIFs", unit="file", leave=True):
        frame_data = imageio.mimread(gif_path, memtest=False)
        frame_rate = imageio.get_reader(gif_path, 'gif').get_meta_data().get('fps', 10)

        frame_results: List[Tuple[np.ndarray, List[List[float]]]] = []
        detection_flags: List[int] = []

        for frame in tqdm(frame_data, desc="Detecting objects", unit="frame", leave=False):
            # 物体検出処理へ
            boxes_and_class_id, flag = process_gif_frame(params, frame)
            frame_results.append((frame, boxes_and_class_id))
            detection_flags.append(flag)

        # 検出されなかったフレームにフラグを補完する処理
        interpolated_flags = interpolate_detection_flags(detection_flags, window_size=3)
        # フラグが補完されたフレームにはバウンディングボックス情報がないので追加する処理
        interpolated_boxes = interpolate_boxes(frame_results, detection_flags, interpolated_flags)

        processed_frames = []
        for (frame, mosaic_boxes_and_class_id), flag in tqdm(zip(interpolated_boxes, interpolated_flags), total=len(interpolated_boxes), desc="Applying mosaic", unit="frame", leave=False):
            if flag:
                mosaic_boxes = [box[:-1] for box in mosaic_boxes_and_class_id]
                # モザイク処理
                processed_frame = apply_circular_mosaic(frame, np.array(mosaic_boxes), params)
            else:
                processed_frame = frame
            processed_frames.append(processed_frame)

            if first_processed_frame is None:
                first_processed_frame = processed_frame

        output_path = os.path.join(params.output_dir, os.path.basename(gif_path))
        imageio.mimsave(output_path, processed_frames, fps=frame_rate)

    return first_processed_frame