"""
jpg, pngファイルを処理する関数群
"""
import cv2
import os
from tqdm import tqdm
from common.util import apply_circular_mosaic
from common.util import apply_segment_mosaic
from common import labels as l
import numpy as np

def save_image(params, original_path, processed_image):
    """画像を保存する処理"""
    output_path = os.path.join(params.output_dir, os.path.basename(original_path))
    # 画質を劣化させずに画像を保存
    cv2.imwrite(
        output_path,
        cv2.cvtColor(processed_image, cv2.COLOR_RGB2BGR),
        [int(cv2.IMWRITE_JPEG_QUALITY), 100]
        )

def process_images(params, file_group):
    """一枚ずつ物体検出、モザイク処理を実行"""
    for file_path in tqdm(file_group, desc="Processing Images", unit="img"):

        segment_target_keys = set(l.SEGMENT_MOSAIC_TARGETS.keys())
        detect_target_keys = set(l.DETECT_MOSAIC_TARGETS.keys())

        # 物体検出を行う
        results = params.model(file_path)

        # SEGMENTモデルでモザイクを行う対象の物体が検出されたかどうかをチェック
        is_segment_mosaic_target_detected = any(
            int(data[-1]) in segment_target_keys
            for result in results
            for data in result.boxes.data
        )

        # DITECTモデルでモザイクを行う対象の物体
        detect_mosaic_targets = np.array([
            data.cpu().numpy()
            for result in results
            for data in result.boxes.data
            if int(data[-1]) in detect_target_keys
        ])

        # 検出された各物体をチェックし、モザイク対象の物体の場合にはそのバウンディングボックスを保存
        mosaic_boxes = []
        if len(detect_mosaic_targets) != 0:
            mosaic_boxes = [
                bbox.tolist() for label, bbox in zip(detect_mosaic_targets[:, -2], detect_mosaic_targets[:, :-2])
            ]

        # Detectモザイク処理
        np_img = cv2.cvtColor(cv2.imread(file_path), cv2.COLOR_BGR2RGB)
        processed_image = apply_circular_mosaic(np_img, mosaic_boxes, params)

        # Segmentモザイク処理
        if is_segment_mosaic_target_detected:
            seg_results = params.segment_model(file_path)
            masks = [ret.masks for ret in seg_results]
            processed_image, segment_detected_count = apply_segment_mosaic(processed_image, masks, params)

        # 画像を保存
        save_image(params, file_path, processed_image)

    return processed_image # UI表示用の画像を返す
