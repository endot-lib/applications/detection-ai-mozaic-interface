import cv2
import os
import numpy as np
from tqdm import tqdm
from typing import List, Tuple, Optional, Union
from common.util import apply_circular_mosaic
from common.params import ProcessingParams

def interpolate_detection_flags(detection_flags: List[int], window_size: int = 3) -> List[int]:
    """物体検出が漏れたフレームを見つけ、それらにモザイク処理を行うためのフラグを立てる関数"""
    interpolated_flags: List[int] = detection_flags.copy()
    half_window: int = window_size // 2

    for i in range(len(detection_flags)):
        if detection_flags[i] == 0:
            window_range: List[int] = detection_flags[max(0, i - half_window):min(len(detection_flags), i + half_window + 1)]
            if sum(window_range) > 0:
                interpolated_flags[i] = 1

    return interpolated_flags


from typing import List, Tuple, Optional

def interpolate_boxes(
    frame_results: List[Tuple[int, List[List[float]]]],
    detection_flags: List[int],
    interpolated_flags: List[int]
) -> List[Tuple[np.ndarray, List[List[float]]]]:
    """フラグが補完されたフレームにはバウンディングボックス情報がないので追加する関数"""
    interpolated_boxes: List[Tuple[np.ndarray, List[List[float]]]] = []

    def find_nearest_boxes(
        frame_results: List[Tuple[np.ndarray, List[List[float]]]],
        detection_flags: List[int],
        start_idx: int
    ) -> Tuple[List[List[float]], Optional[str]]:
        """最も近いフレームのバウンディングボックスを探す関数"""
        distance: int = 1
        while distance < len(frame_results):
            prev_idx: int = max(start_idx - distance, 0)
            next_idx: int = min(start_idx + distance, len(frame_results) - 1)

            if detection_flags[prev_idx] == 1:
                return frame_results[prev_idx][1], 'prev'

            if detection_flags[next_idx] == 1:
                return frame_results[next_idx][1], 'next'

            distance += 1

        return [], None

    def average_boxes(boxes1: List[List[float]], boxes2: List[List[float]]) -> List[List[float]]:
        """バウンディングボックスの平均を計算する関数"""
        return [((np.array(b1) + np.array(b2)) / 2.0).tolist() for b1, b2 in zip(boxes1, boxes2)]

    # フラグが補完されたフレームにはバウンディングボックス情報がないので追加する
    for i in range(len(frame_results)):
        frame_rgb: np.ndarray
        boxes: List[List[float]]
        frame_rgb, boxes = frame_results[i]

        if interpolated_flags[i] == 1:
            if detection_flags[i] == 1:
                interpolated_boxes.append((frame_rgb, boxes))
            else:
                nearest_boxes: List[List[float]]
                position: Optional[str]
                # フラグが立っていないフレームには最も近いフレームのバウンディングボックスを探して追加する
                nearest_boxes, position = find_nearest_boxes(frame_results, detection_flags, i)
                if position == 'prev':
                    interpolated_boxes.append((frame_rgb, nearest_boxes))
                elif position == 'next':
                    interpolated_boxes.append((frame_rgb, nearest_boxes))
                elif position == 'both':
                    box_set: List[List[float]] = average_boxes(nearest_boxes, frame_results[i + 1][1])
                    interpolated_boxes.append((frame_rgb, box_set))
                else:
                    # 両方のフレームにバウンディングボックスがない場合は何もしない
                    interpolated_boxes.append((frame_rgb, []))
        else:
            # フラグが立っていないフレームは何もしない
            interpolated_boxes.append((frame_rgb, boxes))

    return interpolated_boxes


def process_mp4s(params: ProcessingParams, file_group: List[str]) -> Optional[np.ndarray]:
    """複数の動画ファイルを処理する関数"""
    first_processed_frame: Optional[np.ndarray] = None

    # 一つずつ処理する
    for input_file_path in tqdm(file_group, desc="Processing MP4s", unit="file", leave=True):
        cap: cv2.VideoCapture = cv2.VideoCapture(input_file_path)
        if not cap.isOpened():
            raise ValueError(f"Unable to open video file: {input_file_path}")

        fourcc: int = cv2.VideoWriter_fourcc(*'mp4v')
        output_file_path: str = os.path.join(params.output_dir, os.path.basename(input_file_path))
        out: cv2.VideoWriter = cv2.VideoWriter(
            output_file_path,
            fourcc,
            cap.get(cv2.CAP_PROP_FPS),
            (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
        )

        total_frames: int = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

        frame_results: List[Tuple[np.ndarray, List[List[float]]]] = []
        detection_flags: List[int] = []

        for i in tqdm(range(total_frames), desc="Detecting objects", unit="frame", leave=False):
            ret: bool
            frame: np.ndarray
            ret, frame = cap.read()
            if not ret:
                break

            frame_rgb: np.ndarray = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            results = params.model(frame_rgb)
            detection_flags.append(1 if len(results.pred[0]) > 0 else 0)

            mosaic_boxes_and_class_id: List[Union[float, int]] = [
                bbox.tolist() + [int(label.item())] for label, bbox in zip(results.pred[0][:, -1], results.pred[0][:, :-2])
            ]
            # 後続処理に必要な情報を保持
            frame_results.append((i, mosaic_boxes_and_class_id))


        # フレーム間のフラグを補完する処理 [1, 1, 0, 1, 1, 1, ...]とあった場合に0を1にしたい
        interpolated_flags: List[int] = interpolate_detection_flags(detection_flags, 5)

        # 補完されたフラグ情報をもとにさらに補完処理 frame_flat_listをもとに、モザイク処理が必要なフレームにバウンディングボックスを追加する処理
        # フラグが補完されたフレームにはバウンディングボックス情報がないので追加する
        interpolated_boxes: List[Tuple[int, List[List[float]]]] = interpolate_boxes(frame_results, detection_flags, interpolated_flags)

        # 必要なフレームインデックスを取得
        necessary_frame_indices = [frame_index for frame_index, _ in interpolated_boxes]
        # 初期フレームインデックスを設定
        current_frame_index = 0
        cap.set(cv2.CAP_PROP_POS_FRAMES, 0)

        for _ in tqdm(range(total_frames), desc="Processing video", unit="frame", leave=True):
            ret, frame = cap.read()
            if not ret:
                continue

            # 現在のフレームインデックスが必要なフレームインデックスに含まれているか確認
            if current_frame_index in necessary_frame_indices:
                _, mosaic_boxes_and_class_id = interpolated_boxes[current_frame_index]

                frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                # もしmosaic_boxes_and_class_idが空の配列じゃなかったらモザイク処理
                if len(mosaic_boxes_and_class_id) > 0:
                    mosaic_boxes: List[Tuple[int, int, int, int]] = [tuple(box[:-1]) for box in mosaic_boxes_and_class_id]
                    mosaic_frame: np.ndarray = apply_circular_mosaic(frame_rgb, np.array(mosaic_boxes), params)
                else:
                    mosaic_frame = frame_rgb

                out.write(cv2.cvtColor(mosaic_frame, cv2.COLOR_RGB2BGR))

                if first_processed_frame is None:
                    first_processed_frame = mosaic_frame

            current_frame_index += 1

        cap.release()
        out.release()

    return first_processed_frame