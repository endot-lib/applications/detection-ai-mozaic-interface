import os
from termcolor import colored
from common.process_single_image import process_single_image
from common.process_bulk_image import process_bulk_images
from common import labels as l

def process_route(params, img):
    """
    与えられた画像、またはディレクトリに応じて処理を分岐する
    """
    # モデル共通パラメータ設定

    # モザイクを検出するしきい値
    params.model.conf = params.mosaic_threshold
    # モザイクを適用するクラスネームを指定
    params.model.classes = list(l.MOSAIC_TARGETS.keys())
    # NMSで使用されるIntersection over Union（IoU）の閾値です。IoUは検出されたバウンディングボックスの重なりを測定するために使用され、
    # この値が高いほど異なる検出間で重複が少なくなります。
    params.model.iou = 0.1 # default: 0.45
    # クラスに依存しないNMSを行うかどうかを設定します。Trueにすると、オブジェクトのクラスに関係なくNMSが行われます
    params.model.agnostic = False # default False
    # 一つのバウンディングボックスに対して複数のラベルを許可するかどうかを設定します。
    params.model.multi_label = False # default False
    # 一つの画像に対して検出するオブジェクトの最大数です。
    params.model.max_det = 1000 # default 1000
    # 自動混合精度推論（AMP）を使用するかどうかを設定します。これをTrueにすると、計算速度が向上する可能性がありますが、精度が若干落ちる可能性があります。
    params.model.amp = False # default False

    # モザイクを検出するしきい値
    params.segment_model.conf = params.mosaic_threshold
    # モザイクを適用するクラスネームを指定
    params.segment_model.classes = list(l.SEGMENT_MODEL_LABELS.keys())
    # NMSで使用されるIntersection over Union（IoU）の閾値です。IoUは検出されたバウンディングボックスの重なりを測定するために使用され、
    # この値が高いほど異なる検出間で重複が少なくなります。
    params.segment_model.iou = 0.1 # default: 0.45
    # クラスに依存しないNMSを行うかどうかを設定します。Trueにすると、オブジェクトのクラスに関係なくNMSが行われます
    params.segment_model.agnostic = False # default False
    # 一つのバウンディングボックスに対して複数のラベルを許可するかどうかを設定します。
    params.segment_model.multi_label = False # default False
    # 一つの画像に対して検出するオブジェクトの最大数です。
    params.segment_model.max_det = 1000 # default 1000
    # 自動混合精度推論（AMP）を使用するかどうかを設定します。これをTrueにすると、計算速度が向上する可能性がありますが、精度が若干落ちる可能性があります。
    params.segment_model.amp = False # default False

    print(colored(f"Model: {os.path.basename(params.model.model_name)}", "green"))
    print(colored(f"Segment Model: {params.segment_model.ckpt_path}", "green"))

    # 与えられた設定情報を出力
    print(colored(f"Auto Mosaic: {params.auto_mosaic}", "green"))
    print(colored(f"Mosaic Size: {params.mosaic_size}", "green"))
    print(colored(f"Mosaic Threshold: {params.mosaic_threshold}", "green"))
    print(colored(f"Expand Mosaic Range: {params.expand_mosaic_range}", "green"))

    if not params.bulk_mode:  # Single Image mode
        print(colored("Mode: Single mode", "cyan"))
        return process_single_image(params, img)
    else:  # Bulk Images mode
        print(colored("Mode: Bulk mode", "cyan"))
        print(colored(f"Input Dir: {params.input_dir}", "green"))
        print(colored(f"Output Dir: {params.output_dir}", "green"))
        return process_bulk_images(params)
