from common import labels as l
from common.util import determine_auto_mosaic_size
from common.util import apply_circular_mosaic
from common.util import apply_segment_mosaic
from common.util import detected_object_html_text
from termcolor import colored
import numpy as np
from common import labels as l

def process_single_image(params, img):
    """
    単体モードのモザイク処理を実行
    """
    # モザイクのサイズを設定
    if params.auto_mosaic:  # Check if Auto Mosaic Size is enabled
        params.mosaic_size = determine_auto_mosaic_size(img)  # Update mosaic_size if auto_mosaic is True

    segment_target_keys = set(l.SEGMENT_MOSAIC_TARGETS.keys())
    detect_target_keys = set(l.DETECT_MOSAIC_TARGETS.keys())

    # モデルに画像を渡して物体検出を実行
    results = params.model(img)  # Pass RGB image to the model

    # SEGMENTモデルでモザイクを行う対象の物体が検出されたかどうかをチェック
    is_segment_mosaic_target_detected = any(
        int(data[-1]) in segment_target_keys
        for result in results
        for data in result.boxes.data
    )

    # DITECTモデルでモザイクを行う対象の物体
    detect_mosaic_targets = np.array([
        data
        for result in results
        for data in result.boxes.data
        if int(data[-1]) in detect_target_keys
    ])

    # 検出された各物体をチェックし、モザイク対象の物体の場合にはそのバウンディングボックスを保存
    mosaic_boxes = []
    if len(detect_mosaic_targets) != 0:
        mosaic_boxes = [
            bbox.tolist() for label, bbox in zip(detect_mosaic_targets[:, -2], detect_mosaic_targets[:, :-2])
        ]

    # モザイク処理を適用するバウンディングボックスが存在する場合にはモザイク処理を実行
    mosaic_img = apply_circular_mosaic(img, np.array(mosaic_boxes), params) if mosaic_boxes else img

    # SEGMENTモデルでモザイクを行う対象の物体が検出された場合にはモザイク処理を実行
    segment_detected_count = 0
    if is_segment_mosaic_target_detected:
        seg_results = params.segment_model(img)
        masks = [ret.masks for ret in seg_results]
        mosaic_img, segment_detected_count = apply_segment_mosaic(mosaic_img, masks, params)

    message = f"Select mosaic size: {params.mosaic_size}px, \
        {detected_object_html_text(len(mosaic_boxes) + segment_detected_count)}"

    # UI上に出力するメッセージ
    print("Completed Process.")
    return mosaic_img, message, "Processed 1 images"