"""
共通処理をまとめたモジュール
主にモザイク処理に関する処理をまとめている
"""
import cv2
import os
import glob
from typing import Tuple, Dict, List, Union
from common import labels as l
import numpy as np
from common.params import ProcessingParams

def load_models(models_dir: str) -> Tuple[str, str, Dict[str, str]]:
    """
    物体検出を行うモデルを読み込む処理
    """
    model_files: List[str] = [f for f in os.listdir(models_dir) if f.endswith('.pt')]
    model_options: Dict[str, str] = {f: os.path.join(models_dir, f) for f in model_files}
    default_model_name: str = list(model_options.keys())[0]  # Get the name of the first model
    default_model_path: str = list(model_options.values())[0]
    return default_model_name, default_model_path, model_options

def determine_auto_mosaic_size(img: np.ndarray) -> int:
    """
    モザイクサイズを自動で行うにチェックがあった場合の計算式
    画像の長辺が400px以下の場合は固定値を返す
    長辺が400pxを超える場合は、あるモザイク規定に基づいて算出した値を採用する
    """
    h: int
    w: int
    _: int
    h, w, _ = img.shape
    long_edge: int = max(h, w)
    if long_edge >= 400:
        return long_edge // 100
    else:
        return l.DEFAULT_AUTO_MOZAIC_SIZE

def apply_circular_mosaic(
        img: np.ndarray,
        boxes: List[Tuple[int, int, int, int]],
        params: ProcessingParams,
        show_boundary: bool = False
) -> np.ndarray:

    mosaic_img: np.ndarray = img.copy()
    h: int
    w: int
    _: int
    h, w, _ = mosaic_img.shape

    if params.auto_mosaic and max(h, w) >= 400:
        params.mosaic_size = determine_auto_mosaic_size(img)

    for box in boxes:
        x1: int
        y1: int
        x2: int
        y2: int
        x1, y1, x2, y2 = map(int, box)

        x1 = max(0, x1 - params.expand_mosaic_range)
        y1 = max(0, y1 - params.expand_mosaic_range)
        x2 = min(w, x2 + params.expand_mosaic_range)
        y2 = min(h, y2 + params.expand_mosaic_range)

        corner_radius: int = 20
        mask: np.ndarray = np.zeros((h, w), np.uint8)

        cv2.rectangle(mask, (x1 + corner_radius, y1), (x2 - corner_radius, y2), 255, -1)
        cv2.rectangle(mask, (x1, y1 + corner_radius), (x2, y2 - corner_radius), 255, -1)
        cv2.ellipse(mask, (x1 + corner_radius, y1 + corner_radius), (corner_radius, corner_radius), 0, 180, 270, 255, -1)
        cv2.ellipse(mask, (x2 - corner_radius, y1 + corner_radius), (corner_radius, corner_radius), 0, 270, 360, 255, -1)
        cv2.ellipse(mask, (x1 + corner_radius, y2 - corner_radius), (corner_radius, corner_radius), 0, 90, 180, 255, -1)
        cv2.ellipse(mask, (x2 - corner_radius, y2 - corner_radius), (corner_radius, corner_radius), 0, 0, 90, 255, -1)

        # Apply mosaic within the mask
        for i in range(y1, y2, params.mosaic_size):
            for j in range(x1, x2, params.mosaic_size):
                if mask[i, j]:
                    mosaic_cell = mosaic_img[i:min(i + params.mosaic_size, h), j:min(j + params.mosaic_size, w)]
                    color = cv2.mean(mosaic_cell, mask=mask[i:min(i + params.mosaic_size, h), j:min(j + params.mosaic_size, w)])[:3]
                    mosaic_img[i:min(i + params.mosaic_size, h), j:min(j + params.mosaic_size, w)] = color

        # Draw the boundary of the rounded rectangle and overlay it onto the mosaic image if show_boundary is True
        if show_boundary:
            boundary_img = mosaic_img.copy()
            cv2.rectangle(boundary_img, (x1 + corner_radius, y1), (x2 - corner_radius, y2), (0, 255, 0), 1)
            cv2.rectangle(boundary_img, (x1, y1 + corner_radius), (x2, y2 - corner_radius), (0, 255, 0), 1)
            cv2.ellipse(boundary_img, (x1 + corner_radius, y1 + corner_radius), (corner_radius, corner_radius), 0, 180, 270, (0, 255, 0), 1)
            cv2.ellipse(boundary_img, (x2 - corner_radius, y1 + corner_radius), (corner_radius, corner_radius), 0, 270, 360, (0, 255, 0), 1)
            cv2.ellipse(boundary_img, (x1 + corner_radius, y2 - corner_radius), (corner_radius, corner_radius), 0, 90, 180, (0, 255, 0), 1)
            cv2.ellipse(boundary_img, (x2 - corner_radius, y2 - corner_radius), (corner_radius, corner_radius), 0, 0, 90, (0, 255, 0), 1)
            mosaic_img = cv2.addWeighted(mosaic_img, 0.8, boundary_img, 0.2, 0)

    return mosaic_img

def detected_object_html_text(num_detected_objects: int) -> str:
    if num_detected_objects == 0:
        return "<span style='color: red'>No objects detected.</span>"
    return f"<span style='color: aquamarine'>Detected Objects: {num_detected_objects}</span>"

def get_image_files(input_dir: str, extensions: Tuple[str, ...] = ("jpg", "png", "gif", "mp4")) -> List[str]:
    image_files: List[str] = []
    for ext in extensions:
        image_files.extend(glob.glob(os.path.join(input_dir, f"*.{ext}")))
    return image_files

def apply_segment_mosaic(
        img: np.ndarray,
        masks: list,
        params: ProcessingParams,
        show_boundary: bool = False
) -> (np.ndarray, int):

    h, w, _ = img.shape
    mosaic_img = img.copy()

    detected_count = 0

    for mask in masks:
        if mask is not None and hasattr(mask, 'xyn'):

            detected_count = len(mask.xyn)
            for boundary in mask.xyn:
                # 各境界座標を処理
                mask_pixels = np.round(boundary * np.array([w, h])).astype(int)

                # マスク領域を描画
                mask_img = np.zeros((h, w), dtype=np.uint8)
                cv2.fillPoly(mask_img, [mask_pixels], 1)

                # モザイク処理
                for i in range(0, h, params.mosaic_size):
                    for j in range(0, w, params.mosaic_size):
                        if np.any(mask_img[i:i+params.mosaic_size, j:j+params.mosaic_size]):
                            mosaic_img[i:i+params.mosaic_size, j:j+params.mosaic_size] = np.mean(mosaic_img[i:i+params.mosaic_size, j:j+params.mosaic_size], axis=(0, 1)).astype(mosaic_img.dtype)

                # 境界線を描画
                if show_boundary:
                    cv2.polylines(mosaic_img, [mask_pixels], isClosed=True, color=(0, 0, 255), thickness=1)

    return mosaic_img, detected_count