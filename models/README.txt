# README

- yolov8で学習したモデルのみサポートしています（yolov5は不可）
- detection用に学習したモデルは`./detect`ディレクトリへ設置してください
- segment用に学習したモデルは`./segment`ディレクトリへ設置してください