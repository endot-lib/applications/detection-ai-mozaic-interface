@echo off

REM Activate virtual environment
CALL venv\Scripts\activate

REM Run the app
python app.py

REM Deactivate virtual environment (optional)
deactivate
