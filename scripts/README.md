# README

各種スクリプトが入っているディレクトリです。

## スクリプト一覧と詳細

| スクリプト名 | 説明 | README |
|:---|:---|:---|
| [detect_dataset.py](./docs/detect_dataset.md) | データセットの検出を行うスクリプト | [detect_dataset.md](./docs/detect_dataset.md) |
| [clean_directory.py](./docs/clean_directory.md) | データセットの整理を行うスクリプト | [clean_directory.md](./docs/clean_directory.md) |
| [duplicate.py](./docs/duplicate.md) | データセットのかさ増しを行うスクリプト | [duplicate.md](./docs/duplicate.md) |
| [labelme2yolo.py](./docs/labelme2yolo.md) | LabelMeのデータセットをYOLO形式に変換するスクリプト | [labelme2yolo.md](./docs/labelme2yolo.md) |

## 基本的な流れ

1. 学習用の素材を用意する
2. `detect_dataset.py`を実行してアノテーション処理を実行する
  - 自動的にアノテーションされたデータセットが生成される
  - `visualized_images`フォルダ内を選定する
3. `clean_directory.py`を実行してデータセットを整理する
  - `visualized_images`フォルダ内に存在するデータセットだけが`detection_results`に残る
4. `duplicate.py`を実行してかさ増しを行う
5. `labelme2yolo.py`を実行してYOLO形式に変換しつつ、images, labelsのフォルダに分類される（valも生成）
6. 学習を行う

## コマンド例

### detect_dataset.py

```sh
python detect_dataset.py --input_dir="/Users/username/Downloads/testdataset6" --output_dir="/Users/username/Downloads/testdataset6" --model="/Users/username/git/endot-lib/detection-ai-mozaic-interface/models/yolov8-best-seg.pt" --type="segment"
```

### clean_directory.py

```sh
python clean_directory.py --input_dir="/Users/username/Downloads/detects/detect-box1" --use_visualized_images
```

### duplicate.py

```sh
python duplicate.py --input_dir /path/to/dir --transform segment
```

### labelme2yolo.py

```sh
python labelme2yolo.py --json_dir /path/to/dir --seg
```