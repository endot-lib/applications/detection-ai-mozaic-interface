"""
このスクリプトは、指定されたディレクトリ内のファイルを整理するために使用されます。
特に、同名の画像ファイルとテキストファイル（JSONまたはTXT）のペアを保持し、ペアがないファイルを移動します。
この機能は、画像処理プロジェクトやデータセットの整理に特に便利です。
"""
import argparse
import os
from pathlib import Path
from termcolor import colored

def main(directory, use_visualized_images=False):
    image_extensions = {'.jpg', '.jpeg', '.png', '.gif'}
    text_extensions = {'.json', '.txt'}

    # detection_results ディレクトリからファイルを読み込む
    detection_results_dir = os.path.join(directory, "detection_results")
    files = set(os.listdir(detection_results_dir))
    print(colored(f'Total files in detection_results: {len(files)}', 'green'))

    # excluded_images ディレクトリのパスを設定し、存在しない場合は作成
    excluded_dir = os.path.join(directory, "excluded_images")
    if not os.path.exists(excluded_dir):
        os.mkdir(excluded_dir)

    # visualized_images のファイル名を取得
    if use_visualized_images:
        visualized_dir = os.path.join(directory, "visualized_images")
        visualized_files = {Path(f).stem for f in os.listdir(visualized_dir) if Path(f).suffix in image_extensions}
    else:
        visualized_files = None

    images = {Path(f).stem for f in files if Path(f).suffix in image_extensions}
    texts = {Path(f).stem for f in files if Path(f).suffix in text_extensions}

    # classes.txt を保持
    classes_file = "classes.txt"
    if classes_file in files:
        texts.add(Path(classes_file).stem)

    # detection_results ディレクトリ内のファイルを確認し、必要に応じて移動または削除
    for image_base in images:
        if (image_base not in texts) or (visualized_files is not None and image_base not in visualized_files):
            for ext in image_extensions:
                image_path = Path(image_base + ext).name
                if image_path in files:
                    os.rename(os.path.join(detection_results_dir, image_path), os.path.join(excluded_dir, image_path))
                    print(colored(f'Moved to excluded_images: {image_base + ext}', 'yellow'))
                    break

    for text_base in texts:
        if (text_base not in images) or (visualized_files is not None and text_base not in visualized_files):
            for ext in text_extensions:
                text_path = Path(text_base + ext).name
                if text_path in files and text_path != classes_file:
                    os.remove(os.path.join(detection_results_dir, text_path))
                    print(colored(f'Removed: {text_base + ext}', 'red'))
                    break

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="ディレクトリ内の画像とテキストファイルを整理する")
    parser.add_argument('--input_dir', required=True, help="整理するファイルが含まれるディレクトリのパス")
    parser.add_argument('--use_visualized_images', action='store_true', help="visualized_images ディレクトリを基準にファイルを整理する")

    args = parser.parse_args()

    print(colored(f'Input Directory: {args.input_dir}', 'green'))
    main(args.input_dir, args.use_visualized_images)

    print(colored('Complete!', 'green'))
