"""
labelme形式のデータセットを検出するスクリプト
使い方の詳細はREADME.mdを参照
"""
import argparse
import torch
from termcolor import colored
from util.detect_dataset_util.detect_dataset_params import DetectDatasetParam
from util.detect_dataset_util.detect_process import DetectProcess

def main(params: DetectDatasetParam):
    detect_process: DetectProcess = DetectProcess(params)
    detect_process.main_process()

if __name__ == '__main__':

    # 引数の設定
    parser = argparse.ArgumentParser(description='Dataset detection script.')
    parser.add_argument('--input_dir', type=str, required=True, help='Path to the input directory')
    parser.add_argument('--output_dir', type=str, required=True, help='Path to the output directory')
    parser.add_argument('--model_path', type=str, required=True, help='Model to use for detection')
    parser.add_argument('--type', type=str, help='Type of the input data (box or segment)')
    args = parser.parse_args()

    # デバイスの設定
    device: torch.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    # パラメータの設定
    params: DetectDatasetParam = DetectDatasetParam(args.input_dir, args.output_dir, args.model_path, args.type, device)
    # GPUの状態を確認
    gpu_status = 'available' if torch.cuda.is_available() else 'not available'
    # 設定状況を表示
    print(colored('=== Device Information ===', 'green'))
    print(colored(f'GPU is {gpu_status}', 'green'))
    print(colored(f'Using device is {device}', 'green'))
    print(colored('=== Params ===', 'green'))
    print(colored(f'Type: {params.TYPE}', 'green'))
    print(colored(f'Input Directory:  {params.INPUT_DIR}', 'green'))
    print(colored(f'Output Directory: {params.OUTPUT_DIR}', 'green'))
    print(colored(f'Model: {params.MODEL_PATH}', 'green'))

    # 下準備が終わったらメイン処理へ
    main(params)
