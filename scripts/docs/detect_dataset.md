# detect_dataset.py 画像に対して物体検出を行いLabelme形式のデータセットを生成

## 引数

| 引数名 | 説明 | 例 |
| --- | --- | --- |
| --input_dir | データセットを作成したい画像が入ったディレクトリを指定します | `/your/target/input/directory` |
| --output_dir | 生成結果を保存するディレクトリを指定します | `/your/target/output/directory` |
| --model | 物体検出に使用するモデルを指定します | `/your/target/model.pt` |
| --type | 検出タイプ | `box` or `segment` |

## 簡単な解説

### --input_dir

データセットを作成したい画像が入ったディレクトリを指定します。  
サブディレクトリは無視されるので注意してください。

### --output_dir

生成結果を保存するディレクトリを指定します。 
処理結果の重複を避けるため、指定されたディレクトリ内にサブディレクトリをナンバリングして作成します。  

### --model

一般的には`.pt`になります。  
検出する物体はトレーニングされたモデルに依存します。  
検出したくない物体は`detect_dataset_params.py`にて指定してください。  
デフォルトではすべて検出します。

### --type

検出タイプを指定します。  
指定できる値は`box`と`segment`です。  
ただし、**`segment`はsegment用にトレーニングされたモデルでなければ動作しません。**  

- `box`はlabelimgのyolo形式で出力されます（classes.txtは自動生成されます）
- `segment`はlabelmeのセグメンテーション形式で出力されます

### コマンド例

```bash
python detect_dataset.py --input_dir="/Users/username/Downloads/testdataset6" --output_dir="/Users/username/Downloads/testdataset6" --model="/Users/username/git/endot-lib/detection-ai-mozaic-interface/models/yolov8-best-seg.pt" --type="segment"
```

## 生成されるデータセットの構造

- `--output`で指定したディレクトリ直下に`detects`が作成されます
- `detects`の中には`--type`で指定したタイプごとにディレクトリが作成されます
- その中に実行ごとにナンバリングされたディレクトリが作成されます
- その中に`detection_results`、`no_detection_images`、`visualized_images`が作成されます

#### 生成される各ディレクトリの説明

| ディレクトリ名 | 説明 |
| --- | --- |
| detection_results | 検出結果のデータセットが保存されます（トレーニング用） |
| no_detection_images | 検出されなかった画像が保存されます（次回のトレーニング用） |
| visualized_images | 検出結果を可視化した画像が保存されます（画像ビューアで確認しやすいように） |


```txt
detects
├── detect-segment1 # `type`が`segment`の場合は`detect-segment`という名前でディレクトリが作成されます
│   ├── detection_results
│   ├── no_detection_images
│   └── visualized_images
├── detect-segment2 # 実行ごとにナンバリングされます
├── detect-box1 # `type`が`box`の場合は`detect-box`という名前でディレクトリが作成されます
│   ├── detection_results
│   ├── no_detection_images
│   └── visualized_images
├── detect-box2 # 実行ごとにナンバリングされます
├── detect-box3 ...
```