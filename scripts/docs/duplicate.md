# 学習データセットをかさ増しするスクリプト

## 機能概要

学習データセット（画像とjson|textファイルのペア）をかさ増しするスクリプトです。  
指定したディレクトリ内にある学習データセットをいくつかの方法でかさ増しします。  

## 使用方法

```sh
python duplicate.py --input_dir /path/to/dir --transform segment
```

## 引数

| 引数名 | 説明 | 例 |
| --- | --- | --- |
| --input_dir | データセットを作成したい画像が入ったディレクトリを指定します | `/your/target/input/directory` |
| --transform | `segment` or `box`を指定します | `segment` |
