import json
import os
import util.duplicate.segment_transform_types as seg_trans
import util.duplicate.rectangle_transform_types as rect_trans
import util.duplicate.rectangle as rectangle
import util.duplicate.segment as segment
import argparse
from tqdm import tqdm

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--transform', choices=['segment', 'rectangle'], required=True)
    parser.add_argument('--input_dir', required=True)
    # 画像だけをかさ増しするオプションを追加
    parser.add_argument('--image_only', action='store_true')
    args = parser.parse_args()

    input_dir = args.input_dir
    output_dir = os.path.join(input_dir, 'output')
    image_only = args.image_only

    os.makedirs(output_dir, exist_ok=True)

    # セグメント用の変換クラス
    transformers_polygon = [
        seg_trans.Original(),
        seg_trans.LRFlip(),
        seg_trans.Grayscale(),
        seg_trans.Rotate90(),
        seg_trans.Rotate180(),
        # seg_trans.Blur(),
        seg_trans.GaussNoise(),
        seg_trans.MultiplicativeNoise(),
        seg_trans.Rotate180AndFlip(),
        seg_trans.Rotate270(),
        # ... (他のポリゴン用の変換)
    ]

    # バウンディングボックス用の変換クラス
    transformers_rectangle = [
        rect_trans.BBOriginal(),
        rect_trans.BBLRFlip(),
        rect_trans.BBGrayscale(),
        # rect_trans.BBBlur(),
        rect_trans.BBMotionBlur(),
        rect_trans.BBGlassBlur(),
        rect_trans.BBGaussNoise(),
        rect_trans.BBDownscale(),
        rect_trans.BBVerticalFlip(),
        rect_trans.BBRandomScale(),
        rect_trans.BBRotate90(),
        rect_trans.BBRotate180(),
        rect_trans.BBRandomGamma(),
        # ... (他のバウンディングボックス用の変換クラス)
    ]

    for filename in tqdm(os.listdir(input_dir)):
        file_extension = filename.split('.')[-1].lower()
        if file_extension in ['jpg', 'jpeg', 'png']:
            img_path = os.path.join(input_dir, filename)
            try:
                if args.transform == 'segment':
                    segment_output_dir = os.path.join(output_dir, 'segment')
                    os.makedirs(segment_output_dir, exist_ok=True)
                    if image_only:
                        # 画像のみかさ増し処理へ
                        segment.process_files(transformers_polygon, img_path, None, segment_output_dir, None)
                    else:
                        json_path = os.path.join(input_dir, filename.replace(f'.{file_extension}', '.json'))
                        # JSONデータの読み込み
                        with open(json_path, 'r', encoding='utf-8') as f:
                            json_data = json.load(f)
                        # かさ増し処理へ
                        segment.process_files(transformers_polygon, img_path, json_data, segment_output_dir, json_path)
                if args.transform == 'rectangle':
                    txt_path = os.path.join(input_dir, filename.replace(f'.{file_extension}', '.txt'))
                    # テキストデータの読み込み
                    boxes = rectangle.read_txt_file(txt_path)
                    rectangle_output_dir = os.path.join(output_dir, 'rectangle')
                    os.makedirs(rectangle_output_dir, exist_ok=True)
                    # かさ増し処理へ
                    rectangle.process_files_rectangle(transformers_rectangle, img_path, boxes, rectangle_output_dir, txt_path)
            except Exception as e:
                print(f"Error processing file {filename}: {e}")

if __name__ == "__main__":
    main()
