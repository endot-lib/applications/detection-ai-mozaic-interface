import torch
import os
import sys
from termcolor import colored
from ultralytics import YOLO


class DetectDatasetParam:

    def __init__(self, input_dir: str, output_dir: str, model_path: str, type: str, device: torch.device):
        # ここにディレクトリの存在チェック処理を入れる
        self.INPUT_DIR = self.__set_directory_path(input_dir, "Input")
        self.OUTPUT_DIR = self.__set_directory_path(output_dir, "Output")
        self.TYPE = self.__set_type(type)

        # ここにモデルの存在チェック処理とモデルの読み込み処理を入れる
        self.MODEL = self.__set_model(model_path, device)

    def __set_directory_path(self, dir_path: str, dir_type: str) -> str:
        """Set the path to the directory."""
        if self.__check_directory_exists(dir_path, dir_type):
            return dir_path

    def __check_directory_exists(self, directory: str, dir_type: str) -> bool:
        """Check if a given directory exists. If not, print an error message and exit."""
        if not os.path.isdir(directory):
            print(colored(f"Error: {dir_type} directory '{directory}' does not exist.", 'red'))
            sys.exit(1)
        return True

    def __check_model_path_exists(self, model_path: str) -> bool:
        """Check if a given model path exists. If not, print an error message and exit."""
        if not os.path.isfile(model_path):
            print(colored(f"Error: Model '{model_path}' does not exist.", 'red'))
            sys.exit(1)
        self.MODEL_PATH = model_path
        return True

    def __set_model(self, model_path: str, device: torch.device) -> torch.nn.Module:
        """Set the path to the model."""
        if self.__check_model_path_exists(model_path):
            # モデルによって精度が変わるので、モデルの種類によってパラメータを変更すること
            if 'segment' == self.TYPE:
                model = YOLO(model_path).to(device)
                model.conf = 0.2
                model.iou = 0.1
                # model.classes = [0]
                model.imgsz = 640
                # model.eval()
                return model
            if 'box' == self.TYPE:
                model = torch.hub.load('ultralytics/yolov5', 'custom', path=model_path).to(device)
                model.conf = 0.2
                model.iou = 0.1
                # model.classes = [0]
                model.imgsz = 640
                return model

    def __set_type(self, type: str) -> str:
        """Set the type of the input data."""
        if type == "box" or type == "segment":
            return type
        else:
            print(colored(f"Error: Type '{type}' does not usage, (box | segment)", 'red'))
            sys.exit(1)