import os
import cv2
from detect_dataset_params import DetectDatasetParam
from labelme_seg_object import LabelMeSegObject
from labelimg_box_object import LabelImgBoxObject
from termcolor import colored

class DetectProcess:

    params: DetectDatasetParam
    SAVE_DIR_PATH: str = None
    DETECTION_RESULTS_DIR: str = None
    VISUALIZED_IMAGES_DIR: str = None
    NO_DETECTION_DIR: str = None

    def __init__(self, params: DetectDatasetParam):
        self.params: DetectDatasetParam = params

    def main_process(self):
        files = os.listdir(self.params.INPUT_DIR)
        # ディレクトリ作成
        self.__create_save_directory()
        # detection_results, visualized_images, no_detection_images ディレクトリを作成
        self.__create_detection_results_directory()
        self.__create_visualized_images_directory()
        self.__create_no_detection_directory()
        print(colored(f'Total images: {len(files)}', 'green'))
        print(colored(f'Save directory: {self.SAVE_DIR_PATH}', 'green'))

        if 'box' == self.params.TYPE:
            # classes.txtファイルのフルパス
            classes_file_path = os.path.join(self.DETECTION_RESULTS_DIR, 'classes.txt')

        for filename in files:
            no_detection = True  # 検出が行われたかどうかをチェックするフラグ
            _, ext = os.path.splitext(filename)
            if ext.lower() in ['.jpg', '.jpeg', '.png', '.gif']: # 対象の拡張子をリストで定義する
                # 画像を一枚ずつ物体検出する（フルパスへの変換が必要）
                file_path = os.path.join(self.params.INPUT_DIR, filename)
                img = cv2.cvtColor(cv2.imread(file_path), cv2.COLOR_BGR2RGB)
                # 物体検出処理
                results = self.params.MODEL(img)
                print(f'Detected filename: {filename}')

                if 'segment' == self.params.TYPE:

                    # 検出した場合だけ保存
                    if any(len(result.boxes) > 0 for result in results):
                        no_detection = False # 検出が行われたのでフラグをFalseにする
                        # 画像を保存する
                        cv2.imwrite(
                            os.path.join(self.DETECTION_RESULTS_DIR, filename),
                            cv2.cvtColor(img, cv2.COLOR_RGB2BGR),
                            [int(cv2.IMWRITE_JPEG_QUALITY), 100]
                        )
                        # 検出結果を可視化して保存
                        cv2.imwrite(
                            os.path.join(self.SAVE_DIR_PATH, 'visualized_images', filename),
                            cv2.cvtColor(results[0].plot(line_width=2), cv2.COLOR_RGB2BGR),
                        )
                        # 検出結果をjsonに変換して保存する
                        if self.params.TYPE == 'segment':
                            labelme_seg_object = LabelMeSegObject(file_path, results)
                            json = labelme_seg_object.convert_to_labelme().to_json()
                            # JSONファイルを保存
                            with open(os.path.join(self.DETECTION_RESULTS_DIR, f'{os.path.splitext(filename)[0]}.json'), 'w') as outfile:
                                outfile.write(json)

                if 'box' == self.params.TYPE:
                    # classes.txtファイルを保存
                    if not os.path.exists(classes_file_path):
                        with open(classes_file_path, 'w') as f:
                            # 辞書の値（クラス名）のみを改行区切りでファイルに書き込む
                            f.write('\n'.join(self.params.MODEL.names.values()))
                    # 検出結果をYOLO形式に変換して保存する
                    labelimg_box_object = LabelImgBoxObject(file_path, img, results)
                    lines = labelimg_box_object.convert_to_yolo_format()
                    # 物体が検出された場合（linesが空でない場合）のみ保存処理を実行
                    if lines:
                        no_detection = False # 検出が行われたのでフラグをFalseにする
                        # 画像を保存する
                        cv2.imwrite(
                            os.path.join(self.DETECTION_RESULTS_DIR, filename),
                            cv2.cvtColor(img, cv2.COLOR_RGB2BGR),
                            [int(cv2.IMWRITE_JPEG_QUALITY), 100]
                        )
                        # 可視化した画像を保存する
                        draw_bounding_box = labelimg_box_object.draw_bounding_box(img, results, self.params.MODEL.names)
                        cv2.imwrite(
                            os.path.join(self.SAVE_DIR_PATH, 'visualized_images', filename),
                            cv2.cvtColor(draw_bounding_box, cv2.COLOR_RGB2BGR),
                        )

                        # テキストファイルを保存
                        with open(os.path.join(self.DETECTION_RESULTS_DIR, f'{os.path.splitext(filename)[0]}.txt'), 'w') as outfile:
                            outfile.write('\n'.join(lines))
                # No Detection 検出されなかった画像を保存
                if no_detection:
                    cv2.imwrite(
                        os.path.join(self.NO_DETECTION_DIR, filename),
                        cv2.cvtColor(img, cv2.COLOR_RGB2BGR),
                        [int(cv2.IMWRITE_JPEG_QUALITY), 100]
                    )

        print(colored(f'\nSave directory: {self.SAVE_DIR_PATH}', 'green'))
        print(colored('Complete!', 'green'))

    # 保存先ディレクトリを作成する処理
    def __create_save_directory(self) -> None:
        """検出した場合に画像を保存するディレクトリを作成する"""
        # 設定済みの場合はキャッシュ値を返す
        if self.SAVE_DIR_PATH is not None:
            return self.SAVE_DIR_PATH


        detect_dir_name: str = f"detect-{self.params.TYPE}"
        detect_num: int = 1
        # detect1, detect2, ...というディレクトリが存在するか確認し、なければ作成する
        while True:
            # TYPEによってディレクトリ名を変更する boxなら detect-box, segmentなら detect-segment
            detect_name: str = f"{detect_dir_name}{detect_num}"
            detect_path = os.path.join(os.path.join(self.params.OUTPUT_DIR, "detects"), detect_name)

            if not os.path.exists(detect_path):
                os.makedirs(detect_path)
                self.SAVE_DIR_PATH = detect_path
                return self.SAVE_DIR_PATH

            detect_num += 1
    # detection_results ディレクトリを作成する
    def __create_detection_results_directory(self) -> None:
        detection_results_dir: str = os.path.join(self.SAVE_DIR_PATH, "detection_results")
        if not os.path.exists(detection_results_dir):
            os.makedirs(detection_results_dir)
        self.DETECTION_RESULTS_DIR = detection_results_dir

    # visualized_imagesディレクトリを作成する
    def __create_visualized_images_directory(self) -> None:
        """
        座標情報を画像に焼き込んだものを保存するディレクトリを作成する
        ディレクトリはdetectsディレクトリの中のdetectN内作成する
        """
        visualized_images_dir: str = os.path.join(self.SAVE_DIR_PATH, "visualized_images")
        if not os.path.exists(visualized_images_dir):
            os.makedirs(visualized_images_dir)

    # no_detection_imagesディレクトリを作成する
    def __create_no_detection_directory(self) -> None:
        """
        検出されなかった場合に画像を保存するディレクトリを作成します。
        """
        self.NO_DETECTION_DIR = os.path.join(self.SAVE_DIR_PATH, "no_detection_images")
        if not os.path.exists(self.NO_DETECTION_DIR):
            os.makedirs(self.NO_DETECTION_DIR)