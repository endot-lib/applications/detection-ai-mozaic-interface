import cv2
class LabelImgBoxObject:

    # 色のマッピングを定義
    class_colors: tuple = {
        0: (0, 0, 255),  # 赤色
        1: (0, 255, 0),  # 緑色
        2: (255, 0, 0),  # 青色
        3: (0, 0, 0), # 黒色
        # 他のクラスに対しても色を定義
    }

    def __init__(self, file_path: str, img, results):
        self.file_path = file_path
        self.results = results
        self.image_height, self.image_width = img.shape[:2]  # OpenCV画像から高さと幅を取得

    def convert_to_yolo_format(self):
        lines = []
        for box in self.results.pred[0]:  # 最初の画像の検出結果
            class_idx = int(box[-1])  # クラスインデックス
            # xmin, ymin, xmax, ymaxを取得
            xmin, ymin, xmax, ymax = box[:4].tolist()
            # 中心座標と幅、高さに変換
            x_center = ((xmin + xmax) / 2) / self.image_width
            y_center = ((ymin + ymax) / 2) / self.image_height
            width = (xmax - xmin) / self.image_width
            height = (ymax - ymin) / self.image_height
            line = f"{class_idx} {x_center} {y_center} {width} {height}"
            lines.append(line)
        return lines

    # 検出した座標を使ってバウンディングボックスを描画舌画像を作成する処理
    def draw_bounding_box(self, img, results, classes):
        """
        yolov5（torch.hub.load）の場合はsave()関数しかなく(seg版は高機能なplot()がある）、
        これは簡易的なものでファイル名も保存するディレクトリも操作できない
        そのため、検出した座標情報を使って自分でバウンディングボックスを描画して保存する
        """
        for box in results.pred[0]:
            class_idx = int(box[-1])
            class_name = classes[class_idx]
            color = self.class_colors.get(class_idx, (0, 0, 0))  # デフォルトは黒色
            xmin, ymin, xmax, ymax = box[:4].tolist()
            xmin = int(xmin)
            ymin = int(ymin)
            xmax = int(xmax)
            ymax = int(ymax)
            # テキストのサイズを取得
            text_size, baseline = cv2.getTextSize(class_name, cv2.FONT_HERSHEY_SIMPLEX, 0.7, 3)
            text_width, text_height = text_size
            # テキストの背景色の長方形を描画
            cv2.rectangle(img, (xmin, ymin - text_height), (xmin + text_width, ymin), color, -1)
            # バウンディングボックスを描画
            cv2.rectangle(img, (xmin, ymin), (xmax, ymax), color, 2)
            # クラス名を描画
            cv2.putText(img, class_name, (xmin, ymin), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 255, 255), 1)

        return img