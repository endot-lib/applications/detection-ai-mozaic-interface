import json
import os
from ultralytics.engine.results import Results

class LabelMeSegObject:

    def __init__(self, file_path: str, results: Results):
        self.file_path: str = file_path
        self.results: Results = results
        self.image_height, self.image_width = results[0].orig_shape
        self.shapes: list = []

    def convert_to_labelme(self):

        for result in self.results:
            # マスクが存在する場合のみ処理を行う
            if result.masks is not None:
                for i, polygon in enumerate(result.masks.xy):
                    label_idx = result.boxes.cls[i].int().item()  # テンソルから整数値を取得
                    label = result.names[label_idx]  # 辞書からクラス名を取得
                    points = polygon.tolist()  # ポリゴンの頂点をリストに変換
                    self.__add_shape(label, points)
        return self

    # 座標情報と関連する情報を追加する
    def __add_shape(self, label: str, points: list, shape_type="polygon"):
        shape = {
            "label": label,
            "points": points,
            "group_id": None,
            "description": "",
            "shape_type": shape_type,
            "flags": {}
        }
        self.shapes.append(shape)

    # 大枠のjsonを作成する
    def to_json(self):
        data = {
            "version": "5.3.1", # 固定値
            "flags": {},
            "shapes": self.shapes,
            "imagePath": os.path.basename(self.file_path),
            "imageData": None,
            "imageHeight": self.image_height,
            "imageWidth": self.image_width
        }
        return json.dumps(data, indent=4)
