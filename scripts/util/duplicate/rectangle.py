import cv2
import os

def read_txt_file(txt_path):
    with open(txt_path, 'r') as f:
        lines = f.readlines()

    boxes = []
    for line in lines:
        items = line.strip().split()
        class_index = int(items[0])  # クラスインデックスを整数として保持
        other_values = list(map(float, items[1:]))  # 他の値を浮動小数点数として保持
        box = [class_index] + other_values
        boxes.append(box)

    return boxes


def process_files_rectangle(transformers, img_path, boxes, output_dir, txt_path):
    # 画像の読み込み
    image = cv2.imread(img_path)

    for transformer in transformers:
        # 画像とバウンディングボックスの変換
        transformed_image, transformed_boxes = transformer.transform(image, boxes)

        # 出力ファイル名の作成
        extension = img_path.split('.')[-1].lower()  # 拡張子を取得
        img_output_path = os.path.join(output_dir, os.path.basename(img_path).replace(f'.{extension}', f'-{transformer.__class__.__name__}.{extension}'))
        txt_output_path = os.path.join(output_dir, os.path.basename(txt_path).replace('.txt', f'-{transformer.__class__.__name__}.txt'))

        # 画像の保存
        cv2.imwrite(img_output_path, transformed_image)

        # テキストデータの更新
        transformed_txt_output = '\n'.join(' '.join(map(str, bbox)) for bbox in transformed_boxes)

        # テキストファイルの保存
        with open(txt_output_path, 'w') as f:
            f.write(transformed_txt_output)
