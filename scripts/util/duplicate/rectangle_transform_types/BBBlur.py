import albumentations as A

class BBBlur:
    def __init__(self):
        self.transformer = A.Compose([
            A.Blur(blur_limit=(3, 15), p=1),  # p=1 ensures the transform always takes place
        ])

    def transform(self, image, bboxes):
        transformed = self.transformer(image=image)
        return transformed['image'], bboxes  # The bounding boxes remain unchanged
