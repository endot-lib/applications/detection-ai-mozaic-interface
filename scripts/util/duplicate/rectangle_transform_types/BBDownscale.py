import albumentations as A

class BBDownscale:
    def __init__(self):
        self.transformer = A.Compose([
            A.Downscale(scale_min=0.25, scale_max=0.25, p=1),  # p=1 ensures the transform always takes place
        ])

    def transform(self, image, bboxes):
        transformed = self.transformer(image=image)
        return transformed['image'], bboxes  # 座標変換は不要なため、バウンディングボックスのデータはそのまま返します
