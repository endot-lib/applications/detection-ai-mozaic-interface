import albumentations as A

class BBGlassBlur:
    def __init__(self):
        self.transformer = A.Compose([
            A.GlassBlur(sigma=0.7, max_delta=5, iterations=2, always_apply=True),
        ], bbox_params=A.BboxParams(format='pascal_voc', label_fields=['class_labels']))

    def transform(self, image, bboxes):
        # Convert YOLO bboxes to pascal_voc format for albumentations
        h, w = image.shape[:2]
        bboxes_pascal_voc = [[bbox[1]*w - 0.5*bbox[3]*w, bbox[2]*h - 0.5*bbox[4]*h,
                              bbox[1]*w + 0.5*bbox[3]*w, bbox[2]*h + 0.5*bbox[4]*h, bbox[0]] for bbox in bboxes]

        transformed = self.transformer(image=image, bboxes=bboxes_pascal_voc, class_labels=[bbox[0] for bbox in bboxes])

        # Convert pascal_voc bboxes back to YOLO format
        transformed_bboxes_yolo = [[bbox[4],
                                    (bbox[0] + bbox[2]) / (2*w),
                                    (bbox[1] + bbox[3]) / (2*h),
                                    (bbox[2] - bbox[0]) / w,
                                    (bbox[3] - bbox[1]) / h] for bbox in transformed['bboxes']]

        return transformed['image'], transformed_bboxes_yolo
