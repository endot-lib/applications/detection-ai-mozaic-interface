import albumentations as A

class BBGrayscale:
    def __init__(self):
        self.transformer = A.Compose([
            A.ToGray(p=1),  # p=1 ensures the transform always takes place
        ])

    def transform(self, image, bboxes):
        transformed = self.transformer(image=image)
        # albumentationsのToGrayは、グレースケール画像を3D配列として返すので、
        # 追加の処理は必要ありません
        return transformed['image'], bboxes  # 座標変換は不要なため、バウンディングボックスのデータはそのまま返します

