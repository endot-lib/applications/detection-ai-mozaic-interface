class BBOriginal:
    def transform(self, image, bboxes):
        # Convert float class index to int
        bboxes_int_index = [[int(float(bbox[0]))] + bbox[1:] for bbox in bboxes]
        return image, bboxes_int_index  # return the unaltered image and the modified bboxes
