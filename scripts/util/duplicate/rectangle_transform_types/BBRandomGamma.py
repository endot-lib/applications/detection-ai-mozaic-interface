import albumentations as A

class BBRandomGamma:
    def __init__(self, gamma_limit=(200, 400)):
        self.transformer = A.RandomGamma(gamma_limit=gamma_limit)

    def transform(self, image, bboxes):
        augmented = self.transformer(image=image)
        augmented_image = augmented['image']
        # bounding boxes remain the same as gamma transformation doesn't affect the positions
        return augmented_image, bboxes
