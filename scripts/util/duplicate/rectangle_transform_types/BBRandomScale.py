import cv2
import numpy as np

class BBRandomScale:
    def __init__(self, scale_limit=(0.8, 1.2)):
        self.scale_limit = scale_limit

    def transform(self, image, bboxes):
        original_height, original_width = image.shape[:2]
        scale = np.random.uniform(self.scale_limit[0], self.scale_limit[1])
        new_width, new_height = int(original_width * scale), int(original_height * scale)
        scaled_image = cv2.resize(image, (new_width, new_height))

        adjusted_bboxes = []
        for bbox in bboxes:
            class_label, x_center, y_center, width, height = bbox
            # Adjust the bounding box coordinates based on the scaling factor and the original image dimensions
            adjusted_x_center = (x_center * original_width) * scale / new_width
            adjusted_y_center = (y_center * original_height) * scale / new_height
            adjusted_width = (width * original_width) * scale / new_width
            adjusted_height = (height * original_height) * scale / new_height
            adjusted_bboxes.append((class_label, adjusted_x_center, adjusted_y_center, adjusted_width, adjusted_height))

        return scaled_image, adjusted_bboxes