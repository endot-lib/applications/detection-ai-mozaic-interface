import cv2

class BBRotate180:
    def transform(self, image, bboxes):
        # Use cv2.ROTATE_180 to rotate the image 180 degrees
        rotated_image = cv2.rotate(image, cv2.ROTATE_180)
        new_height, new_width = rotated_image.shape[:2]

        adjusted_bboxes = []
        for bbox in bboxes:
            class_label, x_center, y_center, width, height = bbox
            # Adjust the bounding box coordinates for 180 degree rotation
            adjusted_x_center = 1.0 - x_center  # 1.0 - x_center because the image is rotated
            adjusted_y_center = 1.0 - y_center  # 1.0 - y_center because the image is rotated
            adjusted_width = width  # width remains the same
            adjusted_height = height  # height remains the same
            adjusted_bboxes.append((class_label, adjusted_x_center, adjusted_y_center, adjusted_width, adjusted_height))

        return rotated_image, adjusted_bboxes
