import cv2
import json
import os
import base64
import copy
from PIL import Image
import numpy as np

def save_image_with_pillow(img, path):
    # OpenCVの画像（BGR）をPillow形式（RGB）に変換
    img_pillow = Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    img_pillow.save(path)

def process_files(transformers, img_path, json_data, output_dir, json_path):
    # Pillowを使用して画像の読み込み
    pil_image = Image.open(img_path)
    image = np.array(pil_image)

    # OpenCVではBGRを使用するので、RGBからBGRに変換（もし必要な場合）
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

    for transformer in transformers:
        # 画像の変換
        transformed_image = transformer.transform_image(image)
        extension = img_path.split('.')[-1].lower()  # 拡張子を取得
        # 出力ファイル名の作成
        img_output_path = os.path.join(output_dir, os.path.basename(img_path).replace(f'.{extension}', f'-{transformer.__class__.__name__}.{extension}'))

        if json_data and json_path:
            # 画像のみかさ増し処理を行う
            # JSONデータの座標を変換
            data = copy.deepcopy(json_data)  # 元のJSONデータのコピーを作成
            for shape in data['shapes']:
                shape['points'] = [transformer.transform_coordinates(pt, transformed_image) for pt in shape['points']]
            json_output_path = os.path.join(output_dir, os.path.basename(json_path).replace('.json', f'-{transformer.__class__.__name__}.json'))
            # JSONデータの更新
            data['imagePath'] = os.path.basename(img_output_path)
            _, img_data = cv2.imencode(f'.{extension}', transformed_image['image'])  # 拡張子に基づいて画像をエンコード
            data['imageData'] = base64.b64encode(img_data.tobytes()).decode('utf-8')
            data['imageHeight'], data['imageWidth'] = transformed_image['image'].shape[:2]

            # JSONファイルの保存
            with open(json_output_path, 'w') as f:
                json.dump(data, f, indent=2)

        # 画像の保存（Pillowを使用）
        save_image_with_pillow(transformed_image['image'], img_output_path)
