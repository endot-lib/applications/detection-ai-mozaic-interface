import albumentations as A

class Blur:
    @staticmethod
    def transform_image(image):
        augmenter = A.Blur(blur_limit=(1,20), p=1)  # blur_limitでランダムなカーネルサイズを指定
        augmented = augmenter(image=image)
        return augmented

    @staticmethod
    def transform_coordinates(coord, transformed_image_shape):
        return coord  # 座標は変更されない
