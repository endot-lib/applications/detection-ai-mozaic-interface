import albumentations as A

class GaussNoise:
    @staticmethod
    def transform_image(image):
        augmenter = A.GaussNoise(mean=80, p=1)  # var_limitでノイズの強度を指定
        augmented = augmenter(image=image)
        return augmented

    @staticmethod
    def transform_coordinates(coord, transformed_image_shape):
        return coord  # 座標は変更されない
