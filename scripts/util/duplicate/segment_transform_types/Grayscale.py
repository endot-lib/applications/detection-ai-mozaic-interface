import albumentations as A

class Grayscale:
    @staticmethod
    def transform_image(image):
        transform = A.ToGray(p=1)  # p=1 は常に変換を適用することを意味します。
        transformed = transform(image=image)
        return transformed

    @staticmethod
    def transform_coordinates(coord, transformed_image_shape):
        # 座標変換は必要ないため、元の座標をそのまま返します。
        return coord

