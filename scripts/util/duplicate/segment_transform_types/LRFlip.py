import albumentations as A

class LRFlip:
    @staticmethod
    def transform_image(image):
        transform = A.HorizontalFlip(p=1)  # p=1 は常に変換を適用することを意味します。
        transformed = transform(image=image)
        return transformed

    @staticmethod
    def transform_coordinates(coord, transformed_image_shape):
        img_height, img_width, _ = transformed_image_shape['image'].shape
        x, y = coord
        return (img_width - x, y)
