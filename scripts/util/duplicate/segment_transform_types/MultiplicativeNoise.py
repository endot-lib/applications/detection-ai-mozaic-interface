import albumentations as A
import cv2

class MultiplicativeNoise:
    @staticmethod
    def transform_image(image):
        augmenter = A.MultiplicativeNoise(multiplier=[0.5, 1.5], per_channel=True, p=1)
        augmented = augmenter(image=image)
        flipped_image = cv2.flip(augmented['image'], 1)  # 左右反転
        return {'image': flipped_image}

    @staticmethod
    def transform_coordinates(coord, transformed_image_shape):
        img_height, img_width, _ = transformed_image_shape['image'].shape
        x, y = coord
        return (img_width - x, y)  # 左右反転の座標変換
