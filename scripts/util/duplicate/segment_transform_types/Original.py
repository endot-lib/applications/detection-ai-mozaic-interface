class Original:
    @staticmethod
    def transform_image(image):
        return {'image': image}  # 辞書型で画像を返します

    @staticmethod
    def transform_coordinates(coord, transformed_image_shape):
        return coord  # 元の座標をそのまま返します
