import albumentations as A

class Rotate180:
    @staticmethod
    def transform_image(image):
        augmenter = A.Rotate(limit=(180, 180), p=1)  # 180度回転
        augmented = augmenter(image=image)
        return augmented

    @staticmethod
    def transform_coordinates(coord, transformed_image_shape):
        old_x, old_y = coord
        img_height, img_width, _ = transformed_image_shape['image'].shape
        new_x = img_width - old_x - 1
        new_y = img_height - old_y - 1
        return new_x, new_y
