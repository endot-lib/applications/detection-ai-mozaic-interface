import cv2

class Rotate180AndFlip:
    @staticmethod
    def transform_image(image):
        rotated = cv2.rotate(image, cv2.ROTATE_180)  # 180度回転
        flipped = cv2.flip(rotated, 1)  # 左右反転
        return {'image': flipped}

    @staticmethod
    def transform_coordinates(coord, transformed_image_shape):
        old_x, old_y = coord
        img_height, img_width, _ = transformed_image_shape['image'].shape
        new_x = old_x  # 左右反転後のx座標
        new_y = img_height - old_y - 1  # 180度回転後のy座標
        return new_x, new_y
