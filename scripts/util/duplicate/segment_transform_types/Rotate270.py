import cv2

class Rotate270:
    @staticmethod
    def transform_image(image):
        rotated_image = cv2.rotate(image, cv2.ROTATE_90_COUNTERCLOCKWISE)
        return {'image': rotated_image}

    @staticmethod
    def transform_coordinates(coord, transformed_image_shape):
        old_x, old_y = coord
        img_height, img_width = transformed_image_shape['image'].shape[:2]  # 画像の形状にアクセス
        new_x = old_y
        new_y = img_height - old_x - 1
        return new_x, new_y