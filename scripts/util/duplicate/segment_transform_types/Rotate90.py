import albumentations as A

class Rotate90:
    @staticmethod
    def transform_image(image):
        augmenter = A.Transpose(p=1)  # p=1 ensures the transformation is always applied
        transformed = augmenter(image=image)
        return transformed

    @staticmethod
    def transform_coordinates(coord, transformed_image_shape):
        old_x, old_y = coord
        new_x = old_y
        new_y = old_x
        return new_x, new_y
