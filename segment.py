from moviepy.editor import VideoFileClip
import math

def split_video(file_path, num_segments):
    clip = VideoFileClip(file_path)
    duration = clip.duration
    segment_duration = duration / num_segments

    for i in range(num_segments):
        start = i * segment_duration
        end = start + segment_duration
        # 最後のセグメントの場合、終了時間を動画の長さに合わせる
        if i == num_segments - 1:
            end = duration
        subclip = clip.subclip(start, end)
        subclip.write_videofile(f"segment_{i + 1}.mp4", codec="libx264", audio_codec="aac")

# 使い方の例
split_video("/Users/fukumayuuta/Downloads/testdataset5/segment_3.mp4", 6)

