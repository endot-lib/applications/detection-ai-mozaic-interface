@echo off

REM Check if venv directory exists
IF NOT EXIST "venv" (
    echo Creating virtual environment...
    python -m venv venv
)

REM Activate virtual environment and install dependencies
CALL venv\Scripts\activate
venv\Scripts\python.exe -m pip install --upgrade pip  REM
pip install -r requirements.txt


echo.
echo Setup completed. You can now run the app using run.bat
pause
